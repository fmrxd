#ifndef FMRXD_H
#define FMRXD_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <glib.h>

/** The D-Bus name of this server. */
#define BUS_NAME      "com.javispedro.fmrxd"
#define BUS_PATH      "/com/javispedro/fmrxd"
#define BUS_INTERFACE BUS_NAME

/** Whether the server will shutdown when there are no more clients. */
#define SERVER_ON_DEMAND    1
/** Seconds the server will wait before shutting down after last client exits. */
#define SERVER_LINGER_TIME	10

/** Seconds the server will keep the radio device powered on after the last
 client exits. Should be smaller than SERVER_LINGER_TIME. */
#define RADIO_LINGER_TIME   2

/** The BlueZ device ID to power up and configure. */
#define BT_DEV_ID 0

/** The ALSA mixer node to configure. */
#define ALSA_MIXER_NAME "hw:2"
/** The ALSA device from where to read radio audio data. */
#define ALSA_PCM_CAPTURE_NAME "hw:2,0"
/** The audio rate to use. */
#define ALSA_CAPTURE_RATE 48000

/** The size of the global ring buffer size that prevents overruns. */
#define RING_BUFFER_SIZE (ALSA_CAPTURE_RATE)

/** Tuner device to use. */
#define TUNER_DEVICE "/dev/radio0"
#define TUNER_DEVICE_ID 0

/** Enable the wl1273 N950/N9 RDS decoding workarounds. */
#define WORKAROUND_BAD_WL1273_RDS 1

/** Enable the wl1273 N950/N9 signal level workarounds. */
#define WORKAROUND_BAD_WL1273_SIGNAL 1
/** Poll the signal level every this many seconds. */
#define SIGNAL_POLL_INTERVAL 10
/** After tuning, refresh the signal level once after this many miliseconds. */
#define SIGNAL_REFRESH_DELAY 200

/* fmrxd.c -- the main loop */
extern GMainLoop* main_loop;

/* server.c -- the d-bus server, audio server and client logic */
int server_start();
void server_stop();
void server_queue_stop();
int server_new_client();
size_t server_get_buffer(size_t size, void **buffer);
void server_commit_buffer(size_t size);
void server_notify_tuned(double mhz);
void server_notify_stopped();
void server_notify_signal(uint16_t signal);
void server_notify_pi(uint16_t pi);
void server_notify_ps(const char *ps);
void server_notify_rt(const char *rt);

/* radio.c -- enabling and disabling radio logic */
bool radio_active();
void radio_start();
void radio_stop();
void radio_queue_start();
void radio_queue_stop();

/* bt.c -- configuring the wl1273 multiplexer via HCI */
bool configure_bt_muxer(bool on);

/* capture.c -- configure the ALSA mixer and capture */
bool configure_mixer(bool on);
bool configure_capture(bool on);

/* tuner.c  -- setting up the V4L tuner */
bool configure_tuner(bool on);
extern int tuner_fd;
bool tuner_set_frequency(double mhz);
int32_t tuner_get_signal();
bool tuner_search(bool forward);

/* rds.c -- RDS decoder */
bool configure_rds(bool on);
void rds_reset();
unsigned short rds_get_pi();
unsigned char rds_get_pty();
const char * rds_get_pty_text();
gchar * rds_get_ps();
gchar * rds_get_rt();

/* signal.c -- polling signal level */
bool configure_signal(bool on);
void signal_reset();

#endif // FMRXD_H
