CFLAGS?=-Os -Wall -ggdb
LDFLAGS?=-Wl,--as-needed
PREFIX?=/usr
DESTDIR?=

FMRXD_PKGS:=glib-2.0 dbus-glib-1 bluez alsa
FMRXD_CFLAGS:=$(shell pkg-config --cflags $(FMRXD_PKGS))
FMRXD_LIBS:=$(shell pkg-config --libs $(FMRXD_PKGS))

FMRXUTILS_PKGS:=glib-2.0 dbus-glib-1
FMRXUTILS_CFLAGS:=$(shell pkg-config --cflags $(FMRXUTILS_PKGS))
FMRXUTILS_LIBS:=$(shell pkg-config --libs $(FMRXUTILS_PKGS))

all: fmrxd fmrx-cat fmrx-ctl

fmrxd: fmrxd.o server.o radio.o bt.o capture.o tuner.o rds.o signal.o
	$(CC) $(FMRXD_LDFLAGS) $(LDFLAGS) -o $@ $+ $(LIBS) $(FMRXD_LIBS)

fmrx-cat: fmrx-cat.o
	$(CC) $(FMRXUTILS_LDFLAGS) $(LDFLAGS) -o $@ $+ $(LIBS) $(FMRXUTILS_LIBS)

fmrx-ctl: fmrx-ctl.o
	$(CC) $(FMRXUTILS_LDFLAGS) $(LDFLAGS) -o $@ $+ $(LIBS) $(FMRXUTILS_LIBS)

%.o: %.c
	$(CC) $(FMRXD_CFLAGS) $(CFLAGS) -o $@ -c $<

clean:
	rm -f *.o fmrxd fmrx-cat fmrx-ctl

install: fmrxd fmrx-cat fmrx-ctl
	install -d $(DESTDIR)$(PREFIX)/sbin $(DESTDIR)$(PREFIX)/bin
	install -d $(DESTDIR)$(PREFIX)/share/dbus-1/system-services
	install -m 0755 fmrxd $(DESTDIR)$(PREFIX)/sbin
	install -m 0755 fmrx-cat fmrx-ctl $(DESTDIR)$(PREFIX)/bin
	install -m 0644 fmrxd.service \
		$(DESTDIR)$(PREFIX)/share/dbus-1/system-services/com.javispedro.fmrxd.service

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/sbin/fmrxd
	rm -f $(DESTDIR)$(PREFIX)/bin/fmrx-cat $(DESTDIR)$(PREFIX)/bin/fmrx-ctl
	rm -f $(DESTDIR)$(PREFIX)/share/dbus-1/system-services/com.javispedro.fmrxd.service

.PHONY: all clean install uninstall
