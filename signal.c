/*
 *  fmrxd - a daemon to enable and multiplex access to the N950/N9 radio tuner
 *  Copyright (C) 2011 Javier S. Pedro <maemo@javispedro.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "fmrxd.h"

#include <glib.h>
#include <linux/videodev2.h>

static guint signal_timer_id = 0;
static uint16_t last_signal_level = 0;

static void signal_update()
{
	uint16_t new_signal_level;
#if WORKAROUND_BAD_WL1273_SIGNAL
	/* rssi is a value from -128 to +127. */
	new_signal_level = (tuner_get_signal() + 128) * 257;
#else
	/* rssi is a value from 0 to 65535. */
	new_signal_level = tuner_get_signal();
#endif

	if (new_signal_level != last_signal_level) {
		server_notify_signal(new_signal_level);
		last_signal_level = new_signal_level;
	}
}

static gboolean signal_poll(gpointer user_data)
{
	if (!signal_timer_id) return FALSE;
	signal_update();
	return TRUE;
}

static gboolean signal_refresh(gpointer user_data)
{
	if (!signal_timer_id) return FALSE;
	signal_update();
	return FALSE;
}

bool configure_signal(bool on)
{
	if (on) {
		if (!signal_timer_id) {
			signal_timer_id = g_timeout_add_seconds(SIGNAL_POLL_INTERVAL, signal_poll, NULL);
		}
	} else {
		if (signal_timer_id) {
			g_source_remove(signal_timer_id);
			signal_timer_id = 0;
		}
	}

	return true;
}

void signal_reset()
{
	last_signal_level = 0;
	if (signal_timer_id) {
		g_timeout_add(SIGNAL_REFRESH_DELAY, signal_refresh, NULL);
	}
}
