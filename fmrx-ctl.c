#include <stdio.h>
#include <unistd.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

static GMainLoop *main_loop;
static DBusConnection *bus;

static gdouble cmd_tune = 0.0;
static gboolean cmd_next = FALSE;
static gboolean cmd_prev = FALSE;

static GOptionEntry entries[] = {
	{ "tune", 't', 0, G_OPTION_ARG_DOUBLE, &cmd_tune, "Tune to a specific frequency", "MHz" },
	{ "next", 'n', 0, G_OPTION_ARG_NONE, &cmd_next, "Forward scan for a signal", NULL },
	{ "prev", 'p', 0, G_OPTION_ARG_NONE, &cmd_prev, "Backward scan for a signal", NULL },
	{ NULL }
};

#define BUS_NAME      "com.javispedro.fmrxd"
#define BUS_PATH      "/com/javispedro/fmrxd"
#define BUS_INTERFACE BUS_NAME

static void tune(double f)
{
	DBusError err;
	DBusMessage *reply;
	DBusMessage *msg = dbus_message_new_method_call(BUS_NAME, BUS_PATH,
												  BUS_INTERFACE, "Tune");
	g_assert(msg != NULL);

	dbus_error_init(&err);

	dbus_message_append_args(msg, DBUS_TYPE_DOUBLE, &f, DBUS_TYPE_INVALID);

	reply = dbus_connection_send_with_reply_and_block(bus, msg, -1, &err);
	dbus_message_unref(msg);

	if (!reply) {
		g_assert(dbus_error_is_set(&err));
		g_printerr("Tune error: %s", err.message);
		dbus_error_free(&err);
		return;
	}

	dbus_message_unref(reply);
}

static void next()
{
	DBusError err;
	DBusMessage *reply;
	DBusMessage *msg = dbus_message_new_method_call(BUS_NAME, BUS_PATH,
												  BUS_INTERFACE, "SearchForward");
	g_assert(msg != NULL);

	dbus_error_init(&err);

	reply = dbus_connection_send_with_reply_and_block(bus, msg, -1, &err);
	dbus_message_unref(msg);

	if (!reply) {
		g_assert(dbus_error_is_set(&err));
		g_printerr("Tune error: %s", err.message);
		dbus_error_free(&err);
		return;
	}

	dbus_message_unref(reply);
}

static void prev()
{
	DBusError err;
	DBusMessage *reply;
	DBusMessage *msg = dbus_message_new_method_call(BUS_NAME, BUS_PATH,
												  BUS_INTERFACE, "SearchBackward");
	g_assert(msg != NULL);

	dbus_error_init(&err);

	reply = dbus_connection_send_with_reply_and_block(bus, msg, -1, &err);
	dbus_message_unref(msg);

	if (!reply) {
		g_assert(dbus_error_is_set(&err));
		g_printerr("Tune error: %s", err.message);
		dbus_error_free(&err);
		return;
	}

	dbus_message_unref(reply);
}

static void dbus_init()
{
	DBusError err;	dbus_error_init(&err);
	bus = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
	g_assert(bus != NULL);
	g_assert(!dbus_error_is_set(&err));
	dbus_connection_setup_with_g_main(bus, g_main_loop_get_context(main_loop));
}

int main(int argc, char **argv)
{
	GError *error = NULL;
	GOptionContext *context = g_option_context_new("- control the fmrxd daemon");
	main_loop = g_main_loop_new(NULL, FALSE);

	g_option_context_add_main_entries(context, entries, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_printerr("Option parsing failed: %s\n", error->message);
		return 1;
	}

	dbus_init();

	if (cmd_tune > 0.0) {
		tune(cmd_tune);
	}
	if (cmd_prev) {
		prev();
	}
	if (cmd_next) {
		next();
	}

	return 0;
}
