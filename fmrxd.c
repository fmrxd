/*
 *  fmrxd - a daemon to enable and multiplex access to the N950/N9 radio tuner
 *  Copyright (C) 2011 Javier S. Pedro <maemo@javispedro.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdlib.h>
#include <signal.h>
#include <glib.h>

#include "fmrxd.h"

GMainLoop *main_loop;

static gboolean quit_func(gpointer user_data)
{
	g_main_loop_quit(main_loop);
	return FALSE;
}

static void signal_handler(int signal)
{
	// Signal handler; try not to do much stuff here.
	radio_queue_stop();
	g_idle_add(quit_func, NULL);
}

int main(int argc, char **argv)
{
	main_loop = g_main_loop_new(NULL, FALSE);

	/* Trap quit signals to die gracefully. */
	signal(SIGTERM, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGHUP, signal_handler);
	signal(SIGPIPE, SIG_IGN);

	/* Start the D-Bus server. */
	if (server_start() < 0) {
		return 1;
	}

	g_main_loop_run(main_loop);
	return 0;
}
