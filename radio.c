/*
 *  fmrxd - a daemon to enable and multiplex access to the N950/N9 radio tuner
 *  Copyright (C) 2011 Javier S. Pedro <maemo@javispedro.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <glib.h>
#include "fmrxd.h"

static guint start_source_id = 0;
static guint stop_source_id = 0;
static bool active = false;

bool radio_active()
{
	return active;
}

void radio_start()
{
	g_return_if_fail(!active);
	g_debug("Starting radio");

	bool ok;

	ok = configure_bt_muxer(true);
	if (!ok) goto fail_start;

	configure_mixer(true);
	if (!ok) goto fail_start;

	configure_tuner(true);
	if (!ok) goto fail_start;

	configure_capture(true);
	if (!ok) goto fail_start;

	// Not essential.
	configure_signal(true);
	configure_rds(true);

	active = true;
	return; // Success

fail_start:
	g_critical("Radio failed to start!");

	// Try to force a stop
	active = true;
	radio_stop();
}

void radio_stop()
{
	g_return_if_fail(active);
	g_debug("Stopping radio");

	configure_rds(false);
	configure_signal(false);
	configure_capture(false);
	configure_tuner(false);
	configure_mixer(false);
	configure_bt_muxer(false);

	server_notify_stopped();

	active = false;
}

static gboolean radio_start_func(gpointer data)
{
	radio_start();
	start_source_id = 0;
	return FALSE;
}

static gboolean radio_stop_func(gpointer data)
{
	radio_stop();
	stop_source_id = 0;
	return FALSE;
}

void radio_queue_start()
{
	if (stop_source_id) {
		/* If there was a queued stop pending, remove it. */
		g_source_remove(stop_source_id);
	}
	if (!radio_active() && !start_source_id) {
		start_source_id = g_idle_add(radio_start_func, NULL);
	}
}

void radio_queue_stop()
{
	if (start_source_id) {
		/* If there is a queued start, remove it. */
		g_source_remove(start_source_id);
	}
	if (radio_active() && !stop_source_id) {
		stop_source_id = g_timeout_add_seconds(RADIO_LINGER_TIME,
			radio_stop_func, NULL);
	}
}
